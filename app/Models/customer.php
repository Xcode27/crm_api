<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class customer extends Model
{
    use HasFactory, Notifiable;
	
	protected $casts = [
        'educational_background' => 'array',
		'trainings' => 'array',
		'work_experience' => 'array'
    ];
	
	protected $fillable = [
							'firstname',
							'middlename',
							'lastname',
							'house_no_street',
							'brgy',
							'municipality',
							'province',
							'birthdate',
							'age',
							'mobile',
							'email',
							'educational_background',
							'trainings',
							'work_experience',
							'status',
							'createdby',
						  ];
						  
    public function user(){
		
		return $this->belongsTo(User::class);
	}
	
	public function scopeStatus($query, $arg)
    {
        return $query->where('status', $arg);
    }
	
}
