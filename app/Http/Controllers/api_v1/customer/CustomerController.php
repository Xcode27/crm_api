<?php

namespace App\Http\Controllers\api_v1\customer;


use Illuminate\Http\Request;
use App\Http\Controllers\api_v1\BaseController as BaseController;
use App\Http\Traits\CustomerTraits;
use Validator;

class CustomerController extends BaseController
{
    use CustomerTraits;
    public function index(){

        $customer = $this->getAllActiveCustomers('A');
        return $this->sendResponse($customer, 'Customer successfully retrieved.');
    }

    public function createCustomer(Request $request){
		
		try {
			
			$input = $request->all();

			$condition = [
				'firstname' => $request->firstname,
				'middlename' => $request->middlename,
				'lastname' => $request->lastname,
			];
			
			$validator = Validator::make($input, [
				'email' => 'required|email|unique:customers',
			]);

			if($validator->fails()){
				return $this->sendError('Validation Error.', $validator->errors());       
			}
			
			if($this->checkCustomerIfExists($condition) > 0){
				return $this->sendError('Already exists.', ['error'=>'Customer already exists']);
			}
			
			
			$customer = $this->insertCustomer($input);
			
			return $this->sendResponse($customer, 'Customer successfully saved.');
			
		}catch(\Throwable $error){
			return $this->sendError('Error', ['error'=>$error]);
		}
		
    }
	
	public function getCustomerById($id){
		
		$condition = ['id'=>$id];
			
		$customer = $this->getActiveCustomer($condition,'A');
        return $this->sendResponse($customer, 'Customer successfully retrieved.');
	}
	
	public function updateCustomerDetails($id, Request $request){
		try {
			
			$input = $request->all();

			$condition = ['id' => $id];
			
			$validator = Validator::make($input, [
				'email' => 'required|email',
			]);

			if($validator->fails()){
				return $this->sendError('Validation Error.', $validator->errors());       
			}
			
			$customer = $this->updateCustomer($condition,$input);
			
			return $this->sendResponse($customer, 'Customer successfully updated.');
			
		}catch(\Throwable $error){
			return $this->sendError('Error', ['error'=>$error]);
		}
	}
	
	public function removeCustomerData($id){
		try {
			
			$condition = ['id' => $id];
			
			$customer = $this->deleteCustomer($condition);
			
			return $this->sendResponse($customer, 'Customer successfully deleted.');
			
		}catch(\Throwable $error){
			return $this->sendError('Error', ['error'=>$error]);
		}
		
	}
	
	public function sendCustomerEmailNotification(Request $request){
		return $this->sendEmailNotification($request->all());
	}
}
