<?php

namespace App\Http\Controllers\api_v1;

use Illuminate\Http\Request;
use App\Http\Controllers\api_v1\BaseController as BaseController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;

class UserController extends BaseController
{
    //

    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
            $user = Auth::user(); 
			$success['id'] =  $user->id;
            $success['token'] =  $user->createToken('UserToken')->plainTextToken; 
            $success['name'] =  $user->firstname.' '.$user->middlename.' '.$user->lastname;
   
            return $this->sendResponse($success, 'User login successfully.');
        }else{ 
            return $this->sendError('Unauthorized', ['error'=>'Invalid username or password']);
        }
         
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'c_password' => 'required|same:password',
            'userrole' => 'required',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $input['createdby'] = Auth::user()->id;
        $user = User::create($input);
        $success['token'] =  $user->createToken('UserToken')->plainTextToken;
        $success['name'] =  $user->firstname.' '.$user->middlename.' '.$user->lastname;
   
        return $this->sendResponse($success, 'User register successfully.');
    }
	
	
}
