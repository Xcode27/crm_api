<?php
namespace App\Http\Traits;
use DB;
use Auth;

trait helper {

	public function uuidGenerator(){
		if (function_exists('com_create_guid')){
            return com_create_guid();
        }else{
            mt_srand((double)microtime()*10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = chr(123)// "{"
                .substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12)
                .chr(125);// "}"
            return $uuid;
        }
	}
    
    public function insert($table,$data = []){
        $data['created_at'] = \Carbon\Carbon::now();
        $data['updated_at'] = \Carbon\Carbon::now();
        $data['createdby'] = Auth::user()->id;
        $insert = DB::table($table)->insert($data);
        $id = DB::getPdo()->lastInsertId();
        $rec = DB::table($table)->where('id',$id)->first();
        return $rec;
    }

    public function update($table,$data = [],$condition){
        $update_data = DB::table($table)->where($condition)->update($data);
        // $getUpdatedRecord = DB::table($table)->where($condition)->first();
        return true;
    }

    public function getAll($table,$data = [],$condition,$jointables = [],$columns = []){
        $get = DB::table($table);
        if(count($jointables) > 0){
            foreach($jointables as $tables){
                $get->leftJoin(...$tables);
            }
        }
        return $get->select($columns)->get();
    }

    public function getByRecord($table,$data = [],$condition,$jointables = [],$columns = []){
        $rec = DB::table($table)->where($condition);
        if(count($jointables) > 0){
            foreach($jointables as $tables){
                $rec->leftJoin(...$tables);
            }
        }
        return $rec->first();
    }

    public function delete($table,$data = [],$condition){
        $delete = DB::table($table)->where($condition)->delete();

        return true;
    }

    public function recordChecker($table,$condition){
        $counter = DB::table($table)->where($condition)->count();

        return $counter;
    }
	
}