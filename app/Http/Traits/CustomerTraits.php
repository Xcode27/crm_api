<?php
namespace App\Http\Traits;
use DB;
use Auth;
use App\Models\customer;
use App\Models\email;
use Illuminate\Http\Request;
use App\Notifications\CustomerNotification;
use Illuminate\Support\Facades\Notification;


trait CustomerTraits {

	public function insertCustomer($data){
		
		$customer = customer::create($data);
		
		return $customer;
	}
	
	public function getAllActiveCustomers($arg){
		
		$customer = customer::status($arg)->get();
		
		return $customer;
		
	}
	
	public function getActiveCustomer($condition,$arg){
		
		$customer = customer::where($condition)->status($arg)->first();
		
		return $customer;
		
	}
	
	public function checkCustomerIfExists($condition){
		
		$customer = customer::where($condition)->count();
		
		return $customer;
		
	}
	
	public function deleteCustomer($condition){
		$customer = customer::where($condition)->delete();
		
		return 'Success';
	}
	
	public function updateCustomer($condition, $data){
		$customer = customer::where($condition)->update($data);
		return $customer;
	}
	
	public function sendEmailNotification($data){
		
		try{
			
			DB::beginTransaction();
				foreach($data['recipients'] as $recipient){
					$info = [
						'customer_id' => $recipient,
						'email_message' => $data['templateEmail'],
						'is_sent' => true,
						'createdby' => Auth::user()->id
					];
					
					$email = email::create($info);
					
					$condition = ['id'=>$recipient];
					
					$customer = $this->getActiveCustomer($condition,'A');
					$info['name'] = $customer['firstname'].' '.($customer['middlename'] == null ? ' ' : $customer['middlename']).' '.$customer['lastname'];
					$customer->notify(new CustomerNotification($info));
				}
			DB::commit();
			
			return 'success';
		}catch(\Throwable $th){
			return $th;
		}
		
	}
}