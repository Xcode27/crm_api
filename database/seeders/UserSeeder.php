<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id'       => "1",
            'firstname'       => "Juan",
            'middlename'       => "Delacruz",
            'lastname'       => "Delacruz",
            'email'   => 'test@yopmail.com',
            'password'   => Hash::make('123123123'),
            'userrole'   => 0,
            'status'   => 1,
            'createdby'   => "1",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

    }
}
