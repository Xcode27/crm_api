<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstname');
            $table->string('middlename')->nullable();
            $table->string('lastname');
            $table->string('house_no_street')->nullable();
            $table->Integer('brgy');
            $table->Integer('municipality');
            $table->Integer('province');
            $table->date('birthdate');
            $table->Integer('age');
            $table->string('mobile',20);
            $table->string('email')->unique();
			$table->json('educational_background');
			$table->json('trainings');
			$table->json('work_experience');
            $table->bigInteger('createdby');
			$table->enum('status',['A','I']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
