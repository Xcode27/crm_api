<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api_v1\UserController;
use App\Http\Controllers\api_v1\customer\CustomerController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::post('login', [UserController::class, 'login']);

Route::middleware('auth:sanctum')->group( function () {
   // Route::resource('products', ProductController::class);
   Route::post('register', [UserController::class, 'register']);

   Route::get('customer', [CustomerController::class, 'index']);
   Route::post('customer', [CustomerController::class, 'createCustomer']);
   Route::get('customer/{id}', [CustomerController::class, 'getCustomerById']);
   Route::post('customer/{id}', [CustomerController::class, 'updateCustomerDetails']);
   Route::get('removeCustomer/{id}', [CustomerController::class, 'removeCustomerData']);
   Route::post('sendCustomerEmail', [CustomerController::class, 'sendCustomerEmailNotification']);
});
